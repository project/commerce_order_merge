<?php


/**
 * Implements hook_rules_event_info().
 */
function commerce_order_merge_rules_event_info() {
    $events = array();

    $events['commerce_order_merged'] = array(
        'label' => t('Event fired after another order has been merged into this one'),
        'group' => t('Commerce Order'),
        'variables' => array(
            'commerce_order' => array(
                'type' => 'commerce_order',
                'label' => t('Merged order'),
                'skip save' => TRUE,
            ),
        ),
        'access callback' => 'commerce_order_rules_access',
    );

    return $events;
}