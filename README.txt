Commerce Order Merge
===============

Description
-----------

Adds controls in order editing form to merge one order into another.
Moves all product line items and transactions from donor to recipient order,
creates new revisions for order and transactions, deletes donor order at the end.

Dependencies
------------

Drupal Commerce
Entity
Ctools


Configuration
-------------

- Commerce Order Merge permissions

  Home > Administration > People > Permissions
  (admin/people/permissions#module-commerce_order_merge)
  New permission added to manage access to merge controls in order edit form.

